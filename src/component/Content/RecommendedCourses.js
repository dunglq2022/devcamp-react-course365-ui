import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardMedia, Container, Grid} from "@mui/material";
import bannerPost from '../../asset/images/courses/course-reactjs.jpg'
import { grey, lightBlue } from '@mui/material/colors';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import styled from '@emotion/styled';
import avatarTeacher from '../../asset/images/teacher/claire_robertson.jpg'
import BookmarkAddedIcon from '@mui/icons-material/BookmarkAdded';
import { useState } from 'react';

const SaleCourse = styled('span')`
    color: ${grey[400]};
    text-decoration: line-through;
`

const Bookmark = styled(BookmarkAddedIcon)(({ isClicked }) => ({
    color: isClicked ? grey[900] : grey[600],
    cursor: 'pointer',
    transition: 'transform 0.4s ease',
    transform: isClicked ? 'scaleX(1.5) scaleY(1.5)' : 'scaleX(1) scaleY(1)',
  
    '&:hover': {
        transform: 'scaleX(1.5) scaleY(1.5)',
    },
  }));

function RecommentCourse () {
    const [isClicked, setIsClicked] = useState(false);

    const handleClick = () => {
      setIsClicked(!isClicked);
    };
    return (
        <Container maxWidth='xl'>
            <h2 style={{
                    marginTop: '1rem',
                    marginBottom: '1rem'
                }}
            >
                RecommentCourse
            </h2>
            <Grid container spacing={4}>
                <Grid item xs={3}>
                    <Card sx={{ minWidth: 275 }}>
                        <CardMedia
                            component='img'
                            height= 'auto'
                            image={bannerPost}
                        />
                        <CardContent>
                            <Typography sx={{
                                fontWeight: '800',
                                fontSize: '16px',
                                color: lightBlue[700]
                            }}>
                            Getting Started with JavaScript
                            </Typography>
                            <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, mt: 1.5}} color="text.secondary">
                            <AccessTimeIcon sx={{marginRight: '5px'}}/>3h 34m Beginner
                            </Typography>
                            <Typography variant="h6">
                            700$ <SaleCourse>900$</SaleCourse>
                            </Typography>
                        </CardContent>
                        <CardContent
                            sx={{
                                backgroundColor: grey[100],
                               }}
                        >
                            <Grid container sx={{
                                display: 'flex',
                                alignItems: 'center',
                            }}>
                                <Grid item xs={2}>
                                <img src={avatarTeacher} alt='avatar' width={45} style={{
                                    borderRadius:'50%',
                                }}/>
                                </Grid>
                                <Grid item xs={8}>
                                    <Typography>
                                        Clare Robinsons
                                    </Typography>
                                </Grid>
                                <Grid item xs={2}>
                                    <Bookmark isClicked={isClicked} 
                                    onClick={handleClick}
                                    ></Bookmark>
                                </Grid>
                            </Grid>                            
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3}>
                    <Card sx={{ minWidth: 275 }}>
                        <CardMedia
                            component='img'
                            height= 'auto'
                            image={bannerPost}
                        />
                        <CardContent>
                            <Typography sx={{
                                fontWeight: '800',
                                fontSize: '16px',
                                color: lightBlue[700]
                            }}>
                            Getting Started with JavaScript
                            </Typography>
                            <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, mt: 1.5}} color="text.secondary">
                            <AccessTimeIcon sx={{marginRight: '5px'}}/>3h 34m Beginner
                            </Typography>
                            <Typography variant="h6">
                            700$ <SaleCourse>900$</SaleCourse>
                            </Typography>
                        </CardContent>
                        <CardContent
                            sx={{
                                backgroundColor: grey[100],
                               }}
                        >
                            <Grid container sx={{
                                display: 'flex',
                                alignItems: 'center',
                            }}>
                                <Grid item xs={2}>
                                <img src={avatarTeacher} alt='avatar' width={45} style={{
                                    borderRadius:'50%',
                                }}/>
                                </Grid>
                                <Grid item xs={8}>
                                    <Typography>
                                        Clare Robinsons
                                    </Typography>
                                </Grid>
                                <Grid item xs={2}>
                                    <Bookmark isClicked={isClicked} 
                                    onClick={handleClick}
                                    ></Bookmark>
                                </Grid>
                            </Grid>                            
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3}>
                    <Card sx={{ minWidth: 275 }}>
                        <CardMedia
                            component='img'
                            height= 'auto'
                            image={bannerPost}
                        />
                        <CardContent>
                            <Typography sx={{
                                fontWeight: '800',
                                fontSize: '16px',
                                color: lightBlue[700]
                            }}>
                            Getting Started with JavaScript
                            </Typography>
                            <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, mt: 1.5}} color="text.secondary">
                            <AccessTimeIcon sx={{marginRight: '5px'}}/>3h 34m Beginner
                            </Typography>
                            <Typography variant="h6">
                            700$ <SaleCourse>900$</SaleCourse>
                            </Typography>
                        </CardContent>
                        <CardContent
                            sx={{
                                backgroundColor: grey[100],
                               }}
                        >
                            <Grid container sx={{
                                display: 'flex',
                                alignItems: 'center',
                            }}>
                                <Grid item xs={2}>
                                <img src={avatarTeacher} alt='avatar' width={45} style={{
                                    borderRadius:'50%',
                                }}/>
                                </Grid>
                                <Grid item xs={8}>
                                    <Typography>
                                        Clare Robinsons
                                    </Typography>
                                </Grid>
                                <Grid item xs={2}>
                                    <Bookmark isClicked={isClicked} 
                                    onClick={handleClick}
                                    ></Bookmark>
                                </Grid>
                            </Grid>                            
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3}>
                    <Card sx={{ minWidth: 275 }}>
                        <CardMedia
                            component='img'
                            height= 'auto'
                            image={bannerPost}
                        />
                        <CardContent>
                            <Typography sx={{
                                fontWeight: '800',
                                fontSize: '16px',
                                color: lightBlue[700]
                            }}>
                            Getting Started with JavaScript
                            </Typography>
                            <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, mt: 1.5}} color="text.secondary">
                            <AccessTimeIcon sx={{marginRight: '5px'}}/>3h 34m Beginner
                            </Typography>
                            <Typography variant="h6">
                            700$ <SaleCourse>900$</SaleCourse>
                            </Typography>
                        </CardContent>
                        <CardContent
                            sx={{
                                backgroundColor: grey[100],
                               }}
                        >
                            <Grid container sx={{
                                display: 'flex',
                                alignItems: 'center',
                            }}>
                                <Grid item xs={2}>
                                <img src={avatarTeacher} alt='avatar' width={45} style={{
                                    borderRadius:'50%',
                                }}/>
                                </Grid>
                                <Grid item xs={8}>
                                    <Typography>
                                        Clare Robinsons
                                    </Typography>
                                </Grid>
                                <Grid item xs={2}>
                                    <Bookmark isClicked={isClicked} 
                                    onClick={handleClick}
                                    ></Bookmark>
                                </Grid>
                            </Grid>                            
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </Container>
    )
}

export default RecommentCourse;