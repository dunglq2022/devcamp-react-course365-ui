import PopularCourses from './PopularCourses';
import RecommentCourse from './RecommendedCourses';
import SlideComponent from './Slide'
import TrendingCourses from './TrendingCourses';

function ContentComponent () {
    return(
        <div style={{marginBottom: '3rem'}}>
            <div style={{backgroundColor: '#00acc1', height: 'auto'}}>
                <SlideComponent/>
            </div>
            <RecommentCourse/>
            <PopularCourses/>
            <TrendingCourses/>
        </div>
    )
}
export default ContentComponent;