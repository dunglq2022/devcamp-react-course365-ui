import { Box, Grid, Typography, alpha } from '@mui/material';
import logoSlide from '../../asset/images/ionic-207965.png';
import styled from '@emotion/styled';
import { yellow, grey } from '@mui/material/colors';

const secondary = yellow[800]
const primary = grey[100]

const ButtonPrimary = styled('button')`
    background-color: ${(props) => (props.backgroudColor === 'yellow' ? alpha(secondary, 1) : alpha(primary, 1))};
    &:hover {
        background-color: ${(props) => (props.backgroudColor === 'yellow' ? alpha(secondary, 0.75) : alpha(primary, 0.75))};
    }
    cursor: pointer;
    color: ${alpha(grey[900], 1)};
    margin-left: 11em;
    font-size: 18px;
    padding: 10px;
    border: none;
    border-radius: 6px;
`;

function SlideComponent () {
    return(
        <Grid container spacing={3}
            sx={{display: 'flex', margin: "1px", alignItems: 'center'}}
        >
        <Grid item xs={6} 
            sx={{color: 'white'}}
        >
            <Box style={{padding: '0 12em'}}>
                <h1>Welcome to Ionic Course365 Learning Center</h1>
                <Typography>
                Ionic Course365 is an online learning and teaching marketplace with over 5000 courses and 1 million students. Instructor and expertly crafted courses, designed for the modern students and entrepreneur.
                </Typography>
            </Box>
            <Box style={{marginTop: '1rem'}}>
                <ButtonPrimary backgroudColor ='yellow'>Browser Courses</ButtonPrimary>
                <ButtonPrimary style={{marginLeft: '1em'}}>Become an Instructor</ButtonPrimary>
            </Box>
        </Grid>
        <Grid item xs={6}>
            <img src={logoSlide} alt='logo'/>
        </Grid>
    </Grid>
    )
}


export default SlideComponent;