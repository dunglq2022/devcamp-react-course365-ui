import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import logo from '../../asset/images/Ionic_logo.png'
import SearchIcon from '@mui/icons-material/Search';
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import { grey, blue } from '@mui/material/colors';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Fade from '@mui/material/Fade'

const primary = grey[50];
const secondary = grey[900];
const third = blue[500]

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: '5px',
  border: '1px solid grey',
  backgroundColor: alpha(primary, 1),
  '&:hover': {
    backgroundColor: alpha(primary, 0.75),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: alpha(third, 1)
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: alpha(secondary),
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '14ch',
      '&:focus': {
        width: '24ch',
      },
    },
  },
}));

const StyledButton = styled(Button)(({ theme }) => ({
    color: 'gray',
    '&:hover': {
      color: 'black',
    },
  }));

function HeaderComponent () { 
    const [anchorElBrowser, setAnchorElBrowser] = React.useState(null);
    const [anchorElAbout, setAnchorElAbout] = React.useState(null);

    const openBrowser = Boolean(anchorElBrowser);
    const handleClickBrowser = (event) => {
        setAnchorElBrowser(event.currentTarget);
    };
    const handleCloseBrowser = () => {
        setAnchorElBrowser(null);
    };

    const openAbout = Boolean(anchorElAbout)
    const handleClickAbout = (event) => {
        setAnchorElAbout(event.currentTarget);
    };
    const handleCloseAbout = () => {
        setAnchorElAbout(null);
    };
    
    return(
        <>
            <AppBar position="static" style={{backgroundColor: '#f8f9fa'}}>
                <Toolbar disableGutters>
                <img src={logo} alt='logo'/>
                <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                <Button
                    sx={{marginLeft: '5px', color: 'black', textTransform: 'none', fontSize: 16}}
                >
                    Home
                </Button>
                <StyledButton
                    id="fade-button"
                    aria-controls={openBrowser ? 'fade-menu' : undefined}
                    aria-haspopup="true"
                    aria-expanded={openBrowser ? 'true' : undefined}
                    onClick={handleClickBrowser}
                    sx={{textTransform: 'none', fontSize: 16}}
                >
                    Brower Courses
                </StyledButton>
                <Menu
                    id="fade-menu"
                    MenuListProps={{
                    'aria-labelledby': 'fade-button',
                    }}
                    anchorEl={anchorElBrowser}
                    open={openBrowser}
                    onClose={handleCloseBrowser}
                    TransitionComponent={Fade}
                >
                    <MenuItem onClick={handleCloseBrowser}>Web Developer</MenuItem>
                    <MenuItem onClick={handleCloseBrowser}>Mobile App</MenuItem>
                    <MenuItem onClick={handleCloseBrowser}>Desktop App</MenuItem>
                </Menu>

                <StyledButton
                    id="fade-button"
                    aria-controls={openAbout ? 'fade-menu' : undefined}
                    aria-haspopup="true"
                    aria-expanded={openAbout ? 'true' : undefined}
                    onClick={handleClickAbout}
                    sx={{textTransform: 'none', fontSize: 16}}
                >
                    About Us
                </StyledButton>
                <Menu
                    id="fade-menu"
                    MenuListProps={{
                    'aria-labelledby': 'fade-button',
                    }}
                    anchorEl={anchorElAbout}
                    open={openAbout}
                    onClose={handleCloseAbout}
                    TransitionComponent={Fade}
                >
                    <MenuItem onClick={handleCloseAbout}>Vision & Mission</MenuItem>
                    <MenuItem onClick={handleCloseAbout}>Team</MenuItem>
                    <MenuItem onClick={handleCloseAbout}>Contact Us</MenuItem>
                </Menu>
                </Box>

                                
                <Box sx={{ flexGrow: 0 }}>
                    <Search style={{marginRight: 20}}>
                        <SearchIconWrapper>
                        <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                        placeholder="Search code"
                        inputProps={{ 'aria-label': 'search' }}
                        />
                    </Search>
                </Box>
                <Box sx={{ flexGrow: 0, '& button': { m: 1 }}}>
                    <Button
                        variant="outlined"
                        size='small'
                        sx={{padding: '0 1em'}}
                        >
                            <p>Search Courses</p>
                    </Button>
                </Box>
                </Toolbar>
            </AppBar>
        </>
    )
}

export default HeaderComponent;