import styled from "@emotion/styled";
import { Grid, Container } from "@mui/material";
import {lightBlue} from '@mui/material/colors'

const TextLink = styled('a')`
    margin-right: 6px;
    color: ${lightBlue[500]};
    text-decoration: none!important ;
    &:hover{
        cursor: pointer;
        opacity:.6;
    }
`

function FooterComponent () {
    return (
        <Container maxWidth='xl'>
            <Grid container spacing={2}
                sx={{
                                                           
                }}
            >
                <Grid style={{textAlign: 'center', paddingBottom: '3rem'}} item xs={6}>
                © 2023 Ionic Course365. All Rights Reserved.
                </Grid>
                <Grid style={{textAlign: 'right', paddingBottom: '3rem'}} item xs={6}>
                    <TextLink href="#">Privacy</TextLink>
                    <TextLink href="#">Term</TextLink>
                    <TextLink href="#">Feedback</TextLink>
                    <TextLink href="#">Support</TextLink>
                </Grid>
            </Grid>
        </Container>
    )
}

export default FooterComponent;