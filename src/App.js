import ContentComponent from './component/Content/ContentComponent';
import FooterComponent from './component/Footer/Footer';
import HeaderComponent from './component/Header/HeaderComponent';

function App() {
  return (
    <div>
      <HeaderComponent/>
      <ContentComponent/>
      <FooterComponent/>
    </div>
  );
}

export default App;
